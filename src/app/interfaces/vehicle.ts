export interface Vehicle {
  releaseYear: number;
  brand: string;
  model: string;
  price: number;
  engineDescription: string;
  weight: number;
  logo: string;
  image: string;
}
