import { Vehicle } from '../interfaces/vehicle';

export const JAPANESE_CARS: Vehicle[] = [
  {
    releaseYear: 2020,
    brand: 'Honda',
    model: 'Civic Type R',
    price: 40000,
    engineDescription: `The FK2 Civic Type R is powered by the K20C1 Direct Fuel Injection 1,996 cc (2.0 L; 121.8 cu in) 
    turbocharged Inline-four engine with Earth Dreams Technology, having a power output of 310 PS (228 kW; 306 hp) at 6,500 rpm 
    and maximum torque of 400 N⋅m (295 lb⋅ft) at 2,500–4,500 rpm.`,
    weight: 1900,
    logo: 'https://www.pngmart.com/files/1/Honda-Logo-PNG.png',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/2018_Honda_Civic_GT_Type_R_VTEC_2.0_Front.jpg/1920px-2018_Honda_Civic_GT_Type_R_VTEC_2.0_Front.jpg',
  },
  {
    releaseYear: 2008,
    brand: 'Nissan',
    model: '370z',
    price: 30900,
    engineDescription: `At the heart of the Z is the mighty 3.7-litre Nissan VQ V6 engine used in all 370Z variants in South Africa. 
    Seriously powerful, but responsive to the touch, the Variable Valve Event and Lift (VVE) system help the driver control the engine 
    with the deftest of touches. So much so in fact, that even the slightest variation of pressure on the accelerator gives an instant response.`,
    weight: 2300,
    logo: 'https://logodownload.org/wp-content/uploads/2014/09/nissan-logo-1.png',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Nissan_370Z.JPG/1200px-Nissan_370Z.JPG',
  },
  {
    releaseYear: 2020,
    brand: 'Mazda',
    model: 'CX-30',
    price: 22050,
    engineDescription: `This engine comes with a boost of adrenaline. The twin independent variable cam timing 
    (Ti-VCT) technology improves fuel economy while boosting power, all while reducing emissions. The exhilaration 
    is instantaneous when its 450 horsepower and 410 lb.-ft. of torque* springs to life.`,
    weight: 3200,
    logo: 'https://logos-marcas.com/wp-content/uploads/2020/05/Mazda-Logo.png',
    image:
      'https://www.elcarrocolombiano.com/wp-content/uploads/2020/02/20200206-MAZDA-CX-30-COLOMBIA-PRECIO-VERSIONES-CARACTERISTICAS-01A.jpg',
  },
  {
    releaseYear: 2017,
    brand: 'Mitsubishi',
    model: 'Lancer',
    price: 18630,
    engineDescription: `The Mitsubishi Lancer has 1 Diesel Engine and 1 Petrol Engine on offer. The Diesel engine is 1998 cc while the Petrol 
    engine is 1468 cc . It is available with Manual & Automatic transmission.Depending upon the variant and fuel type the Lancer has a mileage 
    of 13.7 to 14.8 kmpl & Ground clearance of Lancer is 185mm. `,
    weight: 2300,
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Mitsubishi_logo.svg/750px-Mitsubishi_logo.svg.png',
    image:
      'https://fallascomunes.com/wp-content/uploads/2022/01/Fallas-Comunes-Del-Mitsubishi-Lancer-2.jpg',
  },
];
