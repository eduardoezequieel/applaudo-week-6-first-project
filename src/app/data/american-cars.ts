import { Vehicle } from '../interfaces/vehicle';

export const AMERICAN_CARS: Vehicle[] = [
  {
    releaseYear: 2022,
    brand: 'Cadillac',
    model: 'CTS',
    price: 1000000,
    engineDescription: `It is available with Automatic transmission. Depending upon the variant and fuel type the CTS has a mileage of 
    14.0 kmpl. The CTS is a 4 seater 4 cylinder car.`,
    weight: 2600,
    logo: 'http://assets.stickpng.com/images/580b585b2edbce24c47b2c46.png',
    image:
      'https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Cadillac-CTS-V_2016_01.jpg?itok=8UgqM-Ue',
  },
  {
    releaseYear: 2022,
    brand: 'Chevrolet',
    model: 'Camaro',
    price: 25000,
    engineDescription: `Available paddle-shift 10-speed automatic transmission
    19-inch low gloss Black-painted forged-aluminum wheels
    Brembo 6-piston front and 4-piston rear Red-painted brake calipers
    Available new Performance copper-free brake system`,
    weight: 2300,
    logo: 'https://logos-marcas.com/wp-content/uploads/2021/03/Chevrolet-Logo.png',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/2/2f/2019_Chevrolet_Camaro_base%2C_front_11.9.19.jpg',
  },
  {
    releaseYear: 2020,
    brand: 'Ford',
    model: 'Mustang GT',
    price: 50000,
    engineDescription: `This engine comes with a boost of adrenaline. The twin independent variable cam timing 
    (Ti-VCT) technology improves fuel economy while boosting power, all while reducing emissions. The exhilaration 
    is instantaneous when its 450 horsepower and 410 lb.-ft. of torque* springs to life.`,
    weight: 3200,
    logo: 'http://assets.stickpng.com/images/580b585b2edbce24c47b2c67.png',
    image:
      'https://cdn.motor1.com/images/mgl/OXBKB/s3/en-el-garage-de-autoblog-ford-mustang-gt-2020.jpg',
  },
  {
    releaseYear: 2018,
    brand: 'Tesla',
    model: 'Model 3',
    price: 55090,
    engineDescription: `Model 3 comes with the option of dual motor all-wheel drive, 20” Überturbine Wheels and Performance Brakes for total 
    control in all weather conditions. A carbon fiber spoiler improves stability at high speeds, all allowing Model 3 to accelerate from 0-60 
    mph* in as little as 3.1 seconds.`,
    weight: 2200,
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Tesla_T_symbol.svg/2056px-Tesla_T_symbol.svg.png',
    image:
      'https://www.km77.com/images/medium/3/8/5/5/tesla-model-3-2021-gran-autonomia-frontal.353855.jpg',
  },
];
