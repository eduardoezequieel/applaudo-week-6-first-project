import { Vehicle } from '../interfaces/vehicle';

export const GERMANY_CARS: Vehicle[] = [
  {
    releaseYear: 2021,
    brand: 'Audi',
    model: 'R8',
    price: 146000,
    engineDescription: `As Audi's top model, the R8 sports car boasts a howling 602-hp V-10 engine just behind its snug two-seat cabin. 
    It shares a lot with the Lamborghini Huracán, but the R8 is less flamboyantly styled. It's also less engaging to drive, but buyers 
    seeking a super car that they can drive daily may find the Audi's refinement appealing. The interior is sparsely equipped, with all
     infotainment functions handled via the digital gauge cluster, but it's still a comfortable and upscale environment worthy of the R8's 
     steep starting price. More cargo space would be appreciated, but that's not why you buy a car like this. `,
    weight: 1200,
    logo: 'http://assets.stickpng.com/images/580b585b2edbce24c47b2c18.png',
    image:
      'https://audimediacenter-a.akamaihd.net/system/production/media/107033/images/cec18ad36f8c5c32332143c9b594974aa0c59c5c/A218765_x500.jpg?1639070140',
  },
  {
    releaseYear: 1992,
    brand: 'Volkswagen',
    model: 'Apollo',
    price: 2000,
    engineDescription: `The Apollo was essentially a re-branded Ford Verona sold from 1990 to 1992. More expensive and marketed as a 
    sportier model, it was only available with the 1.8 L AP engine. Compared with the Verona, it has only minor differences. 
    These are shorter gear ratios, stiffer shock absorbers, different dashboard with orange lighting (unlike the Verona, the Apollo do not 
      share its dashboard with the Escort), smoked tail lights very similar to the ones from the Ford Sapphire, chrome-less rear window frames, 
      fixed-height front seats, painted mirrors (GLS trim) and airfoil. During the first model years, its front turn signals had clear lenses while 
      the Verona had orange lenses. Trim levels were GL (base model) and GLS. A special edition named VIP was released in 1991.`,
    weight: 1800,
    logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Volkswagen_logo_2019.svg/600px-Volkswagen_logo_2019.svg.png  ',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/c/ce/Apollo_guilherme.jpg',
  },
  {
    releaseYear: 2022,
    brand: 'Mercedes-Benz',
    model: 'C Class',
    price: 41600,
    engineDescription: `Petrol C-Classes use either a 1.5-litre or 2.0-litre four-cylinder engine. You'll find the former in the 201bhp C200 
    (and the 168bhp C180, but that's not coming to the UK) and the latter in the 254bhp C300. There are two diesel options too – the C220d and 
    C300d, both 2.0-litres.`,
    weight: 2910,
    logo: 'https://1000marcas.net/wp-content/uploads/2020/10/Logo-Mercedes-Benz.png',
    image:
      'https://www.la.mercedes-benz.com/en/passengercars/mercedes-benz-cars/c-class/_jcr_content/par/textimagecombination/image.MQ6.12.image.20180406093014.jpeg',
  },
  {
    releaseYear: 2015,
    brand: 'Porsche',
    model: '911 GTS',
    price: 130900,
    engineDescription: `The GTS model's twin-turbocharged 3.0-liter flat-six produces 473 horsepower and 420 pound-feet of torque, increases of 
    30 horsepower and 30 pound-feet over the Carrera S. An eight-speed dual-clutch automatic transmission is standard, and a seven-speed manual 
    with a 0.4-inch-shorter shift lever is optional`,
    weight: 2661,
    logo: 'https://logos-world.net/wp-content/uploads/2021/04/Porsche-Logo.png',
    image:
      'https://cdn.autobild.es/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2014/10/360443-porsche-911-carrera-gts-2015-delantera.jpg?itok=UJSAp0H-',
  },
];
