import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() image!: string;
  @Input() logo!: string;
  @Input() brand!: string;
  @Input() model!: string;
  @Input() price!: number;
  @Input() releaseYear!: number;
  @Input() weight!: number;
  @Input() engineDescription!: string;
}
