import { GERMANY_CARS } from './data/germany-cars';
import { Component } from '@angular/core';
import { AMERICAN_CARS } from './data/american-cars';
import { JAPANESE_CARS } from './data/japanese-cars';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  americanCars = AMERICAN_CARS;
  japaneseCars = JAPANESE_CARS;
  germanyCars = GERMANY_CARS;
}
